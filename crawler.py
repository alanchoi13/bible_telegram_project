import requests
import os, json, re
from bs4 import BeautifulSoup


class Crawler:
    def __init__(self, date='2018-04-18'):
        self.__BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.req_result = requests.get('http://qt.swim.org/user_utf/life/user_print_web.php?edit_all=' + date)
        self.words = []
        self.title = None
        self.summary = None
        self.deep_qt = None
        self.pray = None
        self.word_location = None

    def get_words(self, html_contents):
        contents = html_contents.find_all('div', {'style': 'margin-left:10px;'})
        for line in contents[0].contents[1:]:
            self.words.append(line.text.replace('\r', ''))
        return self.words

    def extract_items(self, html_contents):

        content_strong = html_contents.find_all('strong')
        self.word_location = content_strong[1].string
        print(self.word_location)
        content_padding = html_contents.find_all('td', {'class': 'padding'})
        # print(content_padding)
        self.title = content_padding[0].contents[0].replace('\t', '')

        self.summary = content_padding[3].text
        self.pray = content_padding[7].string
        self.deep_qt = content_padding[5].text

        print(self.summary)
        print(self.deep_qt)
        print(self.pray)
        # print(self.title)

    def run(self):
        self.req_result.encoding = "utf-8"
        result = self.req_result
        html = result.text
        life_site_contents = BeautifulSoup(html, 'html.parser')

        words = self.get_words(life_site_contents)
        print(words)
        self.extract_items(life_site_contents)
        # print(life_site_contents.find_all('td', {'class': 'padding'}))


if __name__ == '__main__':
    crawler = Crawler()
    crawler.run()
